@Sample
Feature: Scenarios to resister user on demout

  Scenario Outline: Verify user is able to resister on demout portal (xml data)
    Given I am in demout portal
    When I entered firstName "<firstName>" And lastName "<lastName>" And phone "<phone>" And email "<email>"
    And address "<address>" And city "<city>" And state "<state>" And postalCode "<postalcode>"
    And username "<username>" And password "<password>" And confirmpassword "<confirmpassword>"
    Then I should see "Thank you for registering" message

    Examples: 
      | firstName | lastName | phone      | email       | address   | city      | state     | postalcode | username | password | confirmpassword |
      | ashok     | gual     | 9901439279 | ak@gmai.com | bangalore | bangaluru | karnataka |    5601003 | ak       | gual     | gual            |
