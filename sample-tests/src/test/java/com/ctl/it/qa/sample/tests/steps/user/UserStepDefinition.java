package com.ctl.it.qa.sample.tests.steps.user;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import java.time.Duration;

import org.openqa.selenium.By;


import com.ctl.it.qa.sample.tools.pages.smallbusiness.CompleteResisterPage;
import com.ctl.it.qa.sample.tools.steps.user.UserSteps;
import com.ctl.it.qa.sample.tools.steps.user.VerificationSteps;
import com.ctl.it.qa.staf.Page;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class UserStepDefinition {

	 @Steps
	 UserSteps endUser; //A Step class of Tools project is initialized with @Steps annotation
	 @Steps
	 VerificationSteps fstUser;
	 //HomePage homePage;
	 CompleteResisterPage completeResisterPage;
	 
		@SuppressWarnings("static-access")
		@Given("^I am in demout portal$")
	    public void I_am_in_demout_portal() {
	    	String fileType=Serenity.sessionVariableCalled("file_type");//Serenity feature to store value and which can be used anywhere further in the script
	    	if(fileType.contains("excel")) {
	    		endUser.switchDataTo("sample.xlsx");//Method to switch between different input data
	    	}else {
	    		endUser.switchDataTo("sample.xml");
	    	}
	        endUser.is_in_demout_page();
	    }
	   
		@SuppressWarnings("static-access")
		/*@When("^I entered firstName \"([^\"]*)\"$")
		public void i_entered_firstName(String name) throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
			endUser.register_demout_page(name); 
			Thread.sleep(5000);
		}*/
		@When("^I entered firstName \"([^\"]*)\" And lastName \"([^\"]*)\" And phone \"([^\"]*)\" And email \"([^\"]*)\"$")
		public void i_entered_firstName_And_lastName_And_phone_And_email(String arg1, String arg2, String arg3, String arg4) throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
		    endUser.register_demout_page(arg1, arg2, arg3, arg4);
		}
		


		@When("^address \"([^\"]*)\" And city \"([^\"]*)\" And state \"([^\"]*)\" And postalCode \"([^\"]*)\"$")
		public void address_And_city_And_state_And_postalCode(String arg1, String arg2, String arg3, String arg4) throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
			endUser.register_demout_page1(arg1, arg2, arg3, arg4);
		}

		@When("^username \"([^\"]*)\" And password \"([^\"]*)\" And confirmpassword \"([^\"]*)\"$")
		public void username_And_password_And_confirmpassword(String arg1, String arg2, String arg3) throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
			endUser.register_demout_page2(arg1, arg3, arg3);
		}

		

		/*@When("^I resister in demout portal$")
		public void I_resister_in_demout_portal() {
			endUser.switchDataTo("sample.xml");
			//String url = Page.envData.getFieldValue("sso-console");
			endUser.register_demout_page();
			//endUser.logs_in_sso_as(userType);
		}*/
		
		@Then("^I should see \"([^\"]*)\" message$")
		public void i_should_see_message(String arg1) {
		    // Write code here that turns the phrase above into concrete actions
		    fstUser.is_verified(arg1);
		}
	  }
