package com.ctl.it.qa.sample.tools.steps.user;

import com.ctl.it.qa.sample.tools.pages.smallbusiness.CompleteResisterPage;
import com.ctl.it.qa.sample.tools.steps.CenturylinkSteps;

import net.thucydides.core.annotations.Step;

@SuppressWarnings("serial")
public class VerificationSteps extends CenturylinkSteps{
	
    CompleteResisterPage completeRegisterPage;
    
    @Step
    public boolean is_verified(String expectedMessage) {
    	//completeRegisterPage.lbl_Thank_you_for_registering.getText();
    	boolean isStatusMatch = false;
		if(isExist(completeRegisterPage,30)) {
			isStatusMatch=true;
		}
		return isStatusMatch;
    }
}
