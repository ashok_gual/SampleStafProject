package com.ctl.it.qa.sample.tools.pages.smallbusiness;

import java.time.Duration;

import com.ctl.it.qa.sample.tools.pages.CenturylinkPage;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class CompleteResisterPage  extends CenturylinkPage{

	
	@FindBy(xpath="//font[contains(text(),'Thank you for registering.')]")
	public WebElementFacade lbl_Thank_you_for_registering;
	
	

	@Override
	public WebElementFacade getUniqueElementInPage() {
		// TODO Auto-generated method stub
		return lbl_Thank_you_for_registering;
	}

	}

