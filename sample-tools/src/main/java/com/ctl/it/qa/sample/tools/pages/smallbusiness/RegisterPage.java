package com.ctl.it.qa.sample.tools.pages.smallbusiness;

import java.time.Duration;

import com.ctl.it.qa.sample.tools.pages.CenturylinkPage;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class RegisterPage extends CenturylinkPage {
	
	@FindBy(xpath="//input[@name='firstName']")
	public WebElementFacade tbx_firstname;
	
	@FindBy(xpath="//input[@name='lastName']")
	public WebElementFacade tbx_lasttname;
	
	@FindBy(xpath="//input[@name='phone']")
	public WebElementFacade tbx_phone;
	
	@FindBy(xpath="//input[@id='userName']")
	public WebElementFacade tbx_email;
	
	@FindBy(xpath="//input[@name='address1']")
	public WebElementFacade tbx_address;
	
	@FindBy(xpath="//input[@name='city']")
	public WebElementFacade tbx_city;
	
	@FindBy(xpath="//input[@name='state']")
	public WebElementFacade tbx_state;
	
	@FindBy(xpath="//input[@name='postalCode']")
	public WebElementFacade tbx_postalcode;
	
	@FindBy(xpath="//input[@id='email']")
	public WebElementFacade tbx_username;
	
	@FindBy(xpath="//input[@name='password']")
	public WebElementFacade tbx_password;
	
	@FindBy(xpath="//input[@name='confirmPassword']")
	public WebElementFacade tbx_confpassword;
	
	
	@FindBy(xpath="//input[@name='register']")
	public WebElementFacade btn_submitted;
	
	
	@Override
	public WebElementFacade getUniqueElementInPage() {
		// TODO Auto-generated method stub
		return tbx_firstname;
	}
	

	
}
	/*public void clickOnRegister() {
		try{
		if(btn_submitted.isDisplayed()){
			btn_submitted.click();
		}
		}catch(Exception e){
			
		}}}
	*/


