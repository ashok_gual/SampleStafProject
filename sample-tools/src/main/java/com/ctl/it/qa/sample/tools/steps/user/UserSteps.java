package com.ctl.it.qa.sample.tools.steps.user;

import net.thucydides.core.annotations.Step;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;

import com.ctl.it.qa.sample.tools.pages.smallbusiness.CompleteResisterPage;
import com.ctl.it.qa.sample.tools.pages.smallbusiness.RegisterPage;
import com.ctl.it.qa.sample.tools.steps.CenturylinkSteps;
import com.ctl.it.qa.staf.xml.reader.DataContainer;
import com.ctl.it.qa.staf.xml.reader.IntDataContainer;

@SuppressWarnings("serial")
public class UserSteps extends CenturylinkSteps {
	
     
    RegisterPage resisterPage;

    @Step
    public void is_in_demout_page() {
    	/*
    	 First line: envData is a variable to access Environment data in data input file (excel or xml). Here, url is accessed.
    	 Second line: Maximizing the browser
    	 Third line: shouldExist method checks the page has navigated to the correct page. It check by verifying the WebElementFacade mentioned in method getUniqueElementInPage() in HomePage class exists or not.
    	 */
    	resisterPage.openAt(envData.getFieldValue("url"));
    	resisterPage.maximize();
    	//shouldExist(resisterPage, 30);
    }
    
   
    @Step
	public void register_demout_page(String firstname,String lastname, String phone,String email) throws InterruptedException {
    	/*resisterPage.withTimeoutOf(Duration.ofMillis(6000)).waitFor(resisterPage.tbx_firstname);
		DataContainer dataContainer = (DataContainer) envData.getContainer(
				resisterPage.getClass().getSimpleName()).getContainers();
		fillFields(resisterPage, dataContainer.getMandatoryFields());*/
		//resisterPage.tbx_firstname.click();
    	resisterPage.tbx_firstname.sendKeys(firstname);
    	resisterPage.tbx_lasttname.sendKeys(lastname);
    	resisterPage.tbx_phone.sendKeys(phone);
    	resisterPage.tbx_email.sendKeys(email);
    	Thread.sleep(5000);
    	
    	
	}

    @Step
   	public void register_demout_page1(String address,String city, String state,String postalcode) throws InterruptedException {
       	/*resisterPage.withTimeoutOf(Duration.ofMillis(6000)).waitFor(resisterPage.tbx_firstname);
   		DataContainer dataContainer = (DataContainer) envData.getContainer(
   				resisterPage.getClass().getSimpleName()).getContainers();
   		fillFields(resisterPage, dataContainer.getMandatoryFields());*/
   		//resisterPage.tbx_firstname.click();
       	resisterPage.tbx_address.sendKeys(address);
       	resisterPage.tbx_city.sendKeys(city);
       	resisterPage.tbx_state.sendKeys(state);
       	resisterPage.tbx_postalcode.sendKeys(postalcode);
       	//Thread.sleep(5000);
    
    } 
    @Step
   	public void register_demout_page2(String username,String password, String confirmpassword) throws InterruptedException {
       	/*resisterPage.withTimeoutOf(Duration.ofMillis(6000)).waitFor(resisterPage.tbx_firstname);
   		DataContainer dataContainer = (DataContainer) envData.getContainer(
   				resisterPage.getClass().getSimpleName()).getContainers();
   		fillFields(resisterPage, dataContainer.getMandatoryFields());*/
   		//resisterPage.tbx_firstname.click();
    	JavascriptExecutor js = (JavascriptExecutor)resisterPage.getDriver();  
    	   js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       	resisterPage.tbx_username.sendKeys(username);
       	resisterPage.tbx_password.sendKeys(password);
       	resisterPage.tbx_confpassword.sendKeys(confirmpassword);
        resisterPage.btn_submitted.click();
       
       Thread.sleep(8000);
    
    } 
}